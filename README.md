## Installation
1. Use python 2.7
2. Any version of tensorflow version > 1.0 should be ok.
3. Python packages: matplotlib (>=1.3.1), pillow (>=2.1.0), scipy (>=1.0.0), scikit-image (>=0.13.1), click (>=5.x)
4. Clone the Repo
5. Done

## Demo
```
python run_demo_inference.py 
```

### Train the model:
```
python -u pix_lab/main/train_aru.py &> info.log 
```

### Validate the model:
```
pix_lab/main/validate_ckpt.py
```

    
